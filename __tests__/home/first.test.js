const server = require('../../src/index');
const request = require('supertest');

describe('first tests', () => {

    const token = "b04d19a04f538dcc965c3b874923c8ae9bbf8701";
    const code = "0b3e6bf6411513b51d6830b5e3205924";
    beforeAll(async() => {
    });
    afterAll(async() => {
        server.close();
    })
    it('route get/home', async () => {
    const response = await request(server)
    .get('/home')
    .set('Authorization', `Bearer ${token}`)
    .set('X-Account-Code', code)
    
    
    expect(response.status).toBe(200);

    });
});