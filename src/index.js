const express = require('express');
const app = express();
const routes = require('./router');
const morgan = require('morgan');


const http = require('http');


const cors = require('cors');


const server = http.createServer(app);

server.listen(process.env.PORT || 3000);

app.use(cors())
.use(morgan('combined'))
.set('trust proxy', true)


app.use(express.json());
app.use(routes);




module.exports = app;
