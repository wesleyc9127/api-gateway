const { Router } = require('express');

const router = Router();
const HomeController = require('../controller/homeController')




router
	.get('/', (_req, res) => res.send({ message: 'api running' }))
	.get('/home', HomeController.index);


module.exports = router;