const { config } = require('dotenv');
config();

module.exports = {

	server: {
		port: process.env.SERVER_PORT || 3000
	},
	url :{
		GMS: process.env.GMS || "",
		Login : process.env.LOGIN || ""

	}
};