const express = require('express');
const http = require('http');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const config = require('./config');

const apiRouter = require('../router');



const server = express();

module.exports.configure = () =>
	server
		.use(morgan('combined'))
		.use(cors())
		.use(bodyParser.json())
		.set('trust proxy', true)
		.use(bodyParser.urlencoded({ extended: false }))
		.use(apiRouter);

module.exports.start = async () => 
	http.createServer(server)
		.listen(config.server.port, () => {
			console.log(`Listening at port: ${config.server.port}`);
		});
