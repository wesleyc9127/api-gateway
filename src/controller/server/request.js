const axios = require('axios');
const axiosRetry = require('axios-retry');

axiosRetry(axios, { retries: 3 });

module.exports ={
    async request(urls, methods, headers){
        let responses = [];
        urls.forEach((url, index) => {
          const axiosRequest   = {
            method: methods[index],
            headers: headers,
            url : url
          };
          const res = axios(axiosRequest);
          responses.push(res);
        });
        return responses;

    }
}