
const axios = require('axios');
const config = require('../config/config');
const {request} = require('./server/request')


module.exports ={
    async index(req, res){

        try {
          
        
        const urls =  [`${config.url.GMS}accounts/stats`, `${config.url.Login}permissions`];
        const methods = ['GET', 'GET'];

        

        const headers ={
             authorization: req.headers.authorization,
             'x-account-code' : req.headers['x-account-code'],
             'content-Type': 'application/json'
        };

        let body = 0;

        const response = await request(urls, methods, headers)

    
         await Promise.all(response).then( value =>{
          body = {
        "balance_available": value[0].data?.balance?.available || "",
        "permissions": value[1].data?.response || []
       };
         })
         res.status(200).send(body)
        } catch (error) {
          res.status(400).send(error)
        }
    }
}