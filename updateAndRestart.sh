#!/bin/bash

set -e

cd app/
git pull origin master
npm install
cd src
pm2 stop  apiGateway
pm2 start index.js --name apiGateway
